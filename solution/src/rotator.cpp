#include <LdH/int.hpp>
#include "pixels.hpp"
#include "mem.hpp"

static ImageRotation::rotate_matrix_result _rotate(ImageRotation::pixel_matrix const *in, ImageRotation::pixel_matrix *out);
static ImageRotation::rotate_matrix_result _copy(ImageRotation::pixel_matrix const *in, ImageRotation::pixel_matrix *out);

extern "C" ImageRotation::rotate_matrix_result ImageRotation::rotate(pixel_matrix const *in, rotation_angle angle, pixel_matrix *out)
{
    switch (angle)
    {
        case rotation_angle_0:
            return _copy(in, out);

        case rotation_angle_90:
            return _rotate(in, out);

        case rotation_angle_180:
        {
            pixel_matrix temp;
            switch (_rotate(in, &temp))
            {
                case rotate_matrix_OK:
                    break;
                case rotate_matrix_MEMORY_ERROR:
                    return rotate_matrix_MEMORY_ERROR;
            }
            switch (_rotate(&temp, out))
            {
                case rotate_matrix_OK:
                    free_pixel_matrix(&temp);
                    return rotate_matrix_OK;
                case rotate_matrix_MEMORY_ERROR:
                    free_pixel_matrix(&temp);
                    return rotate_matrix_MEMORY_ERROR;
            }
        }

        case rotation_angle_270:
        {
            pixel_matrix temp1, temp2;
            switch (_rotate(in, &temp1))
            {
                case rotate_matrix_OK:
                    break;
                case rotate_matrix_MEMORY_ERROR:
                    return rotate_matrix_MEMORY_ERROR;
            }
            switch (_rotate(&temp1, &temp2))
            {
                case rotate_matrix_OK:
                    free_pixel_matrix(&temp1);
                    break;
                case rotate_matrix_MEMORY_ERROR:
                    free_pixel_matrix(&temp1);
                    return rotate_matrix_MEMORY_ERROR;
            }
            switch (_rotate(&temp2, out))
            {
                case rotate_matrix_OK:
                    free_pixel_matrix(&temp2);
                    return rotate_matrix_OK;
                case rotate_matrix_MEMORY_ERROR:
                    free_pixel_matrix(&temp2);
                    return rotate_matrix_MEMORY_ERROR;
            }
        }
    }
}

static ImageRotation::rotate_matrix_result _rotate(ImageRotation::pixel_matrix const *in, ImageRotation::pixel_matrix *out)
{
    bool is_matrix_allocated = false;
    *out = ImageRotation::alloc_pixel_matrix(in->height, in->width, &is_matrix_allocated);
    if (!is_matrix_allocated)
        return ImageRotation::rotate_matrix_MEMORY_ERROR;

    LdH::size_t y, x;

    for (y = 0; y < in->height; y++)
    {
        for (x = 0; x < in->width; x++)
            (*out)[in->width - x - 1][y] = (*in)[y][x];
    }

    return ImageRotation::rotate_matrix_OK;
}

static ImageRotation::rotate_matrix_result _copy(ImageRotation::pixel_matrix const *in, ImageRotation::pixel_matrix *out) {
    bool is_matrix_allocated = false;
    *out = ImageRotation::alloc_pixel_matrix(in->width, in->height, &is_matrix_allocated);
    if (!is_matrix_allocated)
        return ImageRotation::rotate_matrix_MEMORY_ERROR;

    LdH::size_t y, x;

    for (y = 0; y < in->height; y++)
    {
        for (x = 0; x < in->width; x++)
            (*out)[y][x] = (*in)[y][x];
    }

    return ImageRotation::rotate_matrix_OK;
}