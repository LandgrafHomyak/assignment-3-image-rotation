#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_IO_HPP
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_IO_HPP

#include <cstdio>
#include <cstring>
#include <algorithm>
#include <LdH/int.hpp>
#include "../pixels.hpp"

namespace ImageRotation::BMP
{
    template<LdH::size_t BITS_PER_PIXEL>
    class pixels_converter_engine
    {
    private:
        static constexpr bool check_is_2_pow(LdH::size_t v) noexcept
        {
            while (v != 0)
            {
                if ((v & 1) != 0)
                {
                    if ((v >> 1) != 0)
                        return false;
                }
                v >>= 1;
            }
            return true;
        }

        static_assert(BITS_PER_PIXEL > 0);
        static_assert((BITS_PER_PIXEL < 8 && check_is_2_pow(BITS_PER_PIXEL)) || (BITS_PER_PIXEL >= 8 && BITS_PER_PIXEL % 8 == 0));

        static constexpr LdH::size_t BYTES_PER_PIXEL = (BITS_PER_PIXEL + 7) / 8;

    public:
        constexpr pixels_converter_engine() = delete;

    private:
        class cstdfile_iterator
        {
        private:
            std::FILE *const file;
            LdH::size_t pos;
            static constexpr LdH::size_t BUFFER_CAPACITY = std::max<LdH::size_t>(1024, BYTES_PER_PIXEL);
            LdH::size_t actual_buffer_size;
            LdH::u8 buffer[BUFFER_CAPACITY];
        public:
            constexpr explicit cstdfile_iterator(std::FILE *f) noexcept: file{f}, pos{0}, actual_buffer_size{0}
            {}

            enum class read_result
            {
                read_OK,
                read_EOF,
                read_ERROR,
            };
            using read_result::read_OK;
            using read_result::read_EOF;
            using read_result::read_ERROR;

            read_result read(LdH::u8 const **const out, LdH::size_t count)
            {
                if (this->pos >= this->actual_buffer_size)
                {
                    this->pos = 0;
                    this->actual_buffer_size = std::fread(&this->buffer, sizeof(LdH::u8), BUFFER_CAPACITY, this->file);
                    if (std::ferror(this->file))
                    {
                        this->actual_buffer_size = 0;
                        return read_ERROR;
                    }
                    if (this->actual_buffer_size == 0 && std::feof(this->file))
                        return read_EOF;
                }
                else if (this->pos + count >= this->actual_buffer_size)
                {
                    std::memmove(this->buffer, &this->buffer[this->pos], this->actual_buffer_size - this->pos);
                    this->actual_buffer_size = (this->actual_buffer_size - this->pos) + std::fread(&this->buffer[this->actual_buffer_size - this->pos], sizeof(LdH::u8), BUFFER_CAPACITY - (this->actual_buffer_size - this->pos), this->file);
                    this->pos = 0;
                    if (std::ferror(this->file))
                    {
                        this->actual_buffer_size = 0;
                        return read_ERROR;
                    }
                    if (this->actual_buffer_size < count && std::feof(this->file))
                        return read_EOF;
                }

                *out = &this->buffer[this->pos];
                this->pos += count;
                return read_OK;
            }

            enum class write_result
            {
                write_OK,
                write_ERROR,
            };
            using write_result::write_OK;
            using write_result::write_ERROR;

            write_result write(LdH::u8 **const in, LdH::size_t count)
            {
                if (this->pos >= BUFFER_CAPACITY)
                {
                    this->pos = 0;
                    if (BUFFER_CAPACITY > std::fwrite(&this->buffer, sizeof(LdH::u8), BUFFER_CAPACITY, this->file))
                        return write_ERROR;

                    if (std::ferror(this->file))
                        return write_ERROR;
                }
                else if (this->pos + count >= this->actual_buffer_size)
                {
                    if (this->pos > std::fwrite(&this->buffer, sizeof(LdH::u8), this->pos, this->file))
                        return write_ERROR;
                    if (std::ferror(this->file))
                        return write_ERROR;
                    this->pos = 0;
                }

                *in = &this->buffer[this->pos];
                this->pos += count;
                return write_OK;
            }
        };


    public:

        class in_byte
        {
        private:

            template<LdH::size_t>
            friend
            class pixels_converter_engine;

            const LdH::u8f value;

            constexpr explicit in_byte(LdH::u8f value) noexcept: value{value}
            {}

        public:
            template<LdH::size_t i>
            [[nodiscard]] constexpr bool bit() const noexcept
            {
                static_assert(BITS_PER_PIXEL > i, "Bit index out of range");
                return ((this->value >> i) & 1) != 0;
            }

            template<LdH::size_t l, LdH::size_t r>
            [[nodiscard]] constexpr LdH::u8f range() const noexcept
            {
                static_assert(l >= r, "Left index must be greater or equals to right");
                static_assert(BITS_PER_PIXEL > l, "Bit index out of range");
                return ((this->value >> r) & ~(~0 << (l - r + 1)));
            }
        };

        class in_bytes_range;

        class in_byte_from_range
        {
        private:

            friend
            class in_bytes_range;

            const LdH::u8f value;

            constexpr explicit in_byte_from_range(LdH::u8f value) noexcept: value{value}
            {}

        public:
            template<LdH::size_t i>
            [[nodiscard]] constexpr bool bit() const noexcept
            {
                static_assert(8 > i, "Bit index out of range");
                return ((this->value >> i) & 1) != 0;
            }

            template<LdH::size_t l, LdH::size_t r>
            [[nodiscard]] constexpr LdH::u8f range() const noexcept
            {
                static_assert(l >= r, "Left index must be greater or equals to right");
                static_assert(8 > l, "Bit index out of range");
                return ((this->value >> r) & ~(~0 << (l - r + 1)));
            }

            [[nodiscard]] constexpr LdH::u8f as_number() const noexcept
            {
                return this->value;
            }
        };

        class in_bytes_range
        {
        private:

            template<LdH::size_t>
            friend
            class pixels_converter_engine;

            LdH::u8f const *const values;

            constexpr explicit in_bytes_range(LdH::u8f const *const values) noexcept: values{values}
            {}

        public:
            template<LdH::size_t i>
            [[nodiscard]] constexpr in_byte_from_range byte() const noexcept
            {
                static_assert(i < BYTES_PER_PIXEL, "Byte index out of range");
                return in_byte_from_range{this->values[i]};
            }
        };

        enum class parse_pixels_result
        {
            parse_pixels_OK,
            parse_pixels_FREAD_ERROR,
            parse_pixels_UNEXPECTED_EOF
        };

        using parse_pixels_result::parse_pixels_OK;
        using parse_pixels_result::parse_pixels_FREAD_ERROR;
        using parse_pixels_result::parse_pixels_UNEXPECTED_EOF;

        template<class map_t>
        static parse_pixels_result parse_pixels(
                std::FILE *const src,
                pixel_matrix *const dst,
                map_t map
        )
        {
            cstdfile_iterator p{src};
            LdH::size_t y, x;
            LdH::u8 const *current_raw_bytes;
            if constexpr (BITS_PER_PIXEL < 8)
            {
                LdH::s8f shift;
                LdH::size_t w = (dst->width + (8 / BITS_PER_PIXEL - 1)) / (8 / BITS_PER_PIXEL);
                LdH::size_t still = dst->width - w;
                LdH::u8f current_byte;
                LdH::size_t row_size_in_bytes = ((dst->width * BITS_PER_PIXEL + 7) / 8);
                LdH::size_t padding_size = ((row_size_in_bytes + 3) / 4 * 4) - row_size_in_bytes;
                for (y = 0; y < dst->height; y++)
                {
                    for (x = 0; x < w;)
                    {
                        switch (p.read(&current_raw_bytes, 1))
                        {
                            case cstdfile_iterator::read_OK:
                                break;
                            case cstdfile_iterator::read_EOF:
                                return parse_pixels_UNEXPECTED_EOF;
                            case cstdfile_iterator::read_ERROR:
                                return parse_pixels_FREAD_ERROR;
                        }
                        current_byte = *current_raw_bytes;
                        for (shift = (LdH::s8f) (8 - BITS_PER_PIXEL); shift >= 0; shift -= BITS_PER_PIXEL)
                            (*dst)[y][x++] = map.deserialize(in_byte(current_byte >> shift));
                    }
                    if (still > 0)
                    {
                        switch (p.read(&current_raw_bytes, 1))
                        {
                            case cstdfile_iterator::read_OK:
                                break;
                            case cstdfile_iterator::read_EOF:
                                return parse_pixels_UNEXPECTED_EOF;
                            case cstdfile_iterator::read_ERROR:
                                return parse_pixels_FREAD_ERROR;
                        }
                        current_byte = *current_raw_bytes;
                        for (shift = (LdH::s8f) (BITS_PER_PIXEL * (still - 1)); shift >= 0; shift -= BITS_PER_PIXEL)
                            (*dst)[y][x++] = map.deserialize(in_byte(current_byte >> shift));
                    }
                    switch (p.read(&current_raw_bytes, padding_size))
                    {
                        case cstdfile_iterator::read_OK:
                            break;
                        case cstdfile_iterator::read_EOF:
                            if (y < dst->height - 1)
                                return parse_pixels_UNEXPECTED_EOF;
                        case cstdfile_iterator::read_ERROR:
                            return parse_pixels_FREAD_ERROR;
                    }
                }
                return parse_pixels_OK;
            }
            else
            {
                LdH::size_t padding_size = ((dst->width * BYTES_PER_PIXEL + 3) / 4 * 4) - dst->width * BYTES_PER_PIXEL;
                for (y = 0; y < dst->height; y++)
                {
                    for (x = 0; x < dst->width; x++)
                    {
                        switch (p.read(&current_raw_bytes, BYTES_PER_PIXEL))
                        {
                            case cstdfile_iterator::read_OK:
                                break;
                            case cstdfile_iterator::read_EOF:
                                return parse_pixels_UNEXPECTED_EOF;
                            case cstdfile_iterator::read_ERROR:
                                return parse_pixels_FREAD_ERROR;
                        }
                        if constexpr (BITS_PER_PIXEL == 8)
                        {
                            (*dst)[y][x] = map.deserialize(in_byte(*current_raw_bytes));
                        }
                        else
                        {
                            (*dst)[y][x] = map.deserialize(in_bytes_range(current_raw_bytes));
                        }
                    }
                    switch (p.read(&current_raw_bytes, padding_size))
                    {
                        case cstdfile_iterator::read_OK:
                            break;
                        case cstdfile_iterator::read_EOF:
                            if (y < dst->height - 1)
                                return parse_pixels_UNEXPECTED_EOF;
                        case cstdfile_iterator::read_ERROR:
                            return parse_pixels_FREAD_ERROR;
                    }
                }
                return parse_pixels_OK;
            }
        }

        class out_byte
        {
        private:

            template<LdH::size_t>
            friend
            class pixels_converter_engine;

            LdH::u8f value;

            constexpr explicit out_byte() noexcept: value{0}
            {}

        public:
            template<LdH::size_t i>
            constexpr void bit(bool new_value) noexcept
            {
                static_assert(BITS_PER_PIXEL > i, "Bit index out of range");
                if (new_value)
                    this->value |= 1 << i;
                else
                    this->value &= ~(1 << i);
            }

            template<LdH::size_t l, LdH::size_t r>
            constexpr void range(LdH::u8f new_value) noexcept
            {
                static_assert(l >= r, "Left index must be greater or equals to right");
                static_assert(BITS_PER_PIXEL > l, "Bit index out of range");
                this->value &= ~(~(~0 << (l - r)) << r);
                this->value |= (new_value & ~(~0 << (l - r))) << r;
            }

            [[nodiscard]] constexpr LdH::u8f as_number() const noexcept
            {
                return this->value;
            }
        };

        class out_bytes_range;

        class out_byte_from_range
        {
        private:

            friend
            class out_bytes_range;

            LdH::u8f *const value;

            constexpr explicit out_byte_from_range(LdH::u8f *const value) noexcept: value{value}
            {}

        public:
            template<LdH::size_t i>
            constexpr void bit(bool new_value) noexcept
            {
                static_assert(BITS_PER_PIXEL > i, "Bit index out of range");
                if (new_value)
                    *(this->value) |= 1 << i;
                else
                    *(this->value) &= ~(1 << i);
            }

            template<LdH::size_t l, LdH::size_t r>
            constexpr void range(LdH::u8f new_value) noexcept
            {
                static_assert(l >= r, "Left index must be greater or equals to right");
                static_assert(BITS_PER_PIXEL > l, "Bit index out of range");
                *(this->value) &= ~(~(~0 << (l - r)) << r);
                *(this->value) |= (new_value & ~(~0 << (l - r))) << r;
            }

            constexpr void from_number(LdH::u8f new_value) noexcept
            {
                *(this->value) = new_value;
            }
        };

        class out_bytes_range
        {
        private:

            template<LdH::size_t>
            friend
            class pixels_converter_engine;

            LdH::u8f *const values;

            constexpr explicit out_bytes_range(LdH::u8f *const values) noexcept: values{values}
            {}

        public:
            template<LdH::size_t i>
            [[nodiscard]] constexpr out_byte_from_range byte() const noexcept
            {
                static_assert(i < BYTES_PER_PIXEL, "Byte index out of range");
                return out_byte_from_range{&this->values[i]};
            }
        };


        enum class dump_pixels_result
        {
            dump_pixels_OK,
            dump_pixels_FWRITE_ERROR,
        };

        using dump_pixels_result::dump_pixels_OK;
        using dump_pixels_result::dump_pixels_FWRITE_ERROR;

        template<class map_t>
        static dump_pixels_result dump_pixels(
                pixel_matrix const *const src,
                std::FILE *const dst,
                map_t map
        )
        {
            cstdfile_iterator p{dst};
            LdH::size_t y, x;
            LdH::u8 *current_raw_bytes;
            if constexpr (BITS_PER_PIXEL < 8)
            {
                LdH::s8f shift;
                LdH::size_t w = (src->width + (8 / BITS_PER_PIXEL - 1)) / (8 / BITS_PER_PIXEL);
                LdH::size_t still = src->width - w;
                LdH::size_t row_size_in_bytes = ((src->width * BITS_PER_PIXEL + 7) / 8);
                LdH::size_t padding_size = ((row_size_in_bytes + 3) / 4 * 4) - row_size_in_bytes;
                for (y = 0; y < src->height; y++)
                {
                    for (x = 0; x < w;)
                    {
                        switch (p.write(&current_raw_bytes, 1))
                        {
                            case cstdfile_iterator::write_OK:
                                break;
                            case cstdfile_iterator::write_ERROR:
                                return dump_pixels_FWRITE_ERROR;
                        }
                        for (shift = (LdH::s8f) (8 - BITS_PER_PIXEL); shift >= 0; shift -= BITS_PER_PIXEL)
                        {
                            out_byte b{};
                            map.serialize((*src)[y][x++], &b);
                            (*current_raw_bytes) |= b.as_number() << shift;
                        }
                    }
                    if (still > 0)
                    {
                        switch (p.write(&current_raw_bytes, 1))
                        {
                            case cstdfile_iterator::write_OK:
                                break;
                            case cstdfile_iterator::write_ERROR:
                                return dump_pixels_FWRITE_ERROR;
                        }
                        for (shift = (LdH::s8f) (BITS_PER_PIXEL * (still - 1)); shift >= 0; shift -= BITS_PER_PIXEL)
                        {
                            out_byte b{};
                            map.serialize((*src)[y][x++], &b);
                            (*current_raw_bytes) |= b.as_number() << shift;
                        }
                    }
                    switch (p.write(&current_raw_bytes, padding_size))
                    {
                        case cstdfile_iterator::write_OK:
                            break;
                        case cstdfile_iterator::write_ERROR:
                            return dump_pixels_FWRITE_ERROR;
                    }
                }
                return dump_pixels_OK;
            }
            else
            {
                LdH::size_t padding_size = ((src->width * BYTES_PER_PIXEL + 3) / 4 * 4) - src->width * BYTES_PER_PIXEL;
                for (y = 0; y < src->height; y++)
                {
                    for (x = 0; x < src->width; x++)
                    {
                        switch (p.write(&current_raw_bytes, BYTES_PER_PIXEL))
                        {
                            case cstdfile_iterator::write_OK:
                                break;
                            case cstdfile_iterator::write_ERROR:
                                return dump_pixels_FWRITE_ERROR;
                        }
                        if constexpr (BITS_PER_PIXEL == 8)
                        {
                            out_byte b{};
                            map.serialize((*src)[y][x], &b);
                            (*current_raw_bytes) = b.as_number();
                        }
                        else
                        {
                            out_bytes_range br{current_raw_bytes};
                            map.serialize((*src)[y][x], &br);
                        }
                    }
                    switch (p.write(&current_raw_bytes, padding_size))
                    {
                        case cstdfile_iterator::write_OK:
                            break;
                        case cstdfile_iterator::write_ERROR:
                            return dump_pixels_FWRITE_ERROR;
                    }
                }
                return dump_pixels_OK;
            }
        }
    };


    class map_pixel1
    {
    public:
        map_pixel1() = default;

        pixel deserialize(pixels_converter_engine<1>::in_byte raw)
        {
            if (raw.bit<0>())
                return pixel{0xFF, 0xFF, 0xFF};
            else
                return pixel{0, 0, 0};
        }

        void serialize(pixel in, pixels_converter_engine<1>::out_byte *out)
        {
            if (in.red() == 0 && in.green() == 0 && in.blue() == 0)
                out->bit<0>(false);
            else
                out->bit<0>(true);
        }
    };

    class map_pixel16
    {
    public:
        map_pixel16() = default;

        pixel deserialize(pixels_converter_engine<16>::in_bytes_range raw)
        {
            LdH::u8f blue = raw.byte<0>().range<7, 3>();
            LdH::u8f green = (raw.byte<0>().range<3, 0>() << 2) | raw.byte<1>().range<7, 5>();
            LdH::u8f red = raw.byte<1>().range<5, 0>();
            return pixel{(LdH::u8f) (red << 3), (LdH::u8f) (green << 3), (LdH::u8f) (blue << 3)};
        }


        void serialize(pixel in, pixels_converter_engine<16>::out_bytes_range *out)
        {
            out->byte<0>().range<7, 3>(in.blue() >> 3);
            out->byte<0>().range<3, 0>(in.green() >> 5);
            out->byte<1>().range<7, 5>(in.green() >> 3);
            out->byte<1>().range<5, 0>(in.red() >> 3);
        }
    };

    class map_pixel24
    {
    public:
        map_pixel24() = default;

        pixel deserialize(pixels_converter_engine<24>::in_bytes_range raw)
        {
            LdH::u8f blue = raw.byte<0>().as_number();
            LdH::u8f green = raw.byte<1>().as_number();
            LdH::u8f red = raw.byte<2>().as_number();
            return pixel{red, green, blue};
        }


        void serialize(pixel in, pixels_converter_engine<24>::out_bytes_range *out)
        {
            out->byte<0>().from_number(in.blue());
            out->byte<1>().from_number(in.green());
            out->byte<2>().from_number(in.red());
        }
    };

    class map_pixel32
    {
    public:
        map_pixel32() = default;

        pixel deserialize(pixels_converter_engine<32>::in_bytes_range raw)
        {
            LdH::u8f blue = raw.byte<0>().as_number();
            LdH::u8f green = raw.byte<1>().as_number();
            LdH::u8f red = raw.byte<2>().as_number();
            return pixel{red, green, blue};
        }

        void serialize(pixel in, pixels_converter_engine<32>::out_bytes_range *out)
        {
            out->byte<0>().from_number(in.blue());
            out->byte<1>().from_number(in.green());
            out->byte<2>().from_number(in.red());
        }
    };
}

#endif // ASSIGNMENT_3_IMAGE_ROTATION_BMP_IO_HPP
