#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_HEADERS_HPP
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_HEADERS_HPP

#include <cstdio>
#include <algorithm>
#include <LdH/int.hpp>
#include <LdH/struct_serializer.hpp>

namespace ImageRotation
{
    namespace BMP
    {
        namespace Headers
        {
            constexpr LdH::u16 FILE_MAGIC = 0x4D42;

            struct BITMAPFILEHEADER
            {
            public:
                LdH::u16f bfType;
                LdH::u32f bfSize;
                LdH::u32f reserved;
                LdH::u32f bfOffBits;
                LdH::u32f headerSize;
            private:
                static constexpr auto _struct = LdH::StructSerializer::Struct<BITMAPFILEHEADER>(
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U16, &BITMAPFILEHEADER::bfType>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPFILEHEADER::bfSize>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPFILEHEADER::reserved>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPFILEHEADER::bfOffBits>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPFILEHEADER::headerSize>()
                );
            public:
                static constexpr LdH::size_t RAW_SIZE = _struct.size;

                template<class buffer_t>
                static void serialize(BITMAPFILEHEADER const *object, buffer_t buffer, LdH::size_t offset = 0) noexcept
                {
                    _struct.serialize(object, buffer, offset);
                }

                template<class buffer_t>
                static void deserialize(BITMAPFILEHEADER *object, const buffer_t buffer, LdH::size_t offset = 0) noexcept
                {
                    _struct.deserialize(object, buffer, offset);
                }
            };

            struct BITMAPCOREHEADER
            {
            public:
                static constexpr LdH::u32f VERSION = 12;

                //LdH::u32f bcSize;
                LdH::u16f bcWidth;
                LdH::u16f bcHeight;
                LdH::u16f bcPlanes;
                LdH::u16f bcBitCount;
            private:
                static constexpr auto _struct = LdH::StructSerializer::Struct<BITMAPCOREHEADER>(
                        //LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPCOREHEADER::bcSize>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U16, &BITMAPCOREHEADER::bcWidth>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U16, &BITMAPCOREHEADER::bcHeight>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U16, &BITMAPCOREHEADER::bcPlanes>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U16, &BITMAPCOREHEADER::bcBitCount>()
                );
            public:
                static constexpr LdH::size_t RAW_SIZE = _struct.size;

                template<class buffer_t>
                static void serialize(BITMAPCOREHEADER const *object, buffer_t buffer, LdH::size_t offset = 0) noexcept
                {
                    _struct.serialize(object, buffer, offset);
                }

                template<class buffer_t>
                static void deserialize(BITMAPCOREHEADER *object, const buffer_t buffer, LdH::size_t offset = 0) noexcept
                {
                    _struct.deserialize(object, buffer, offset);
                }
            };

            struct BITMAPINFOHEADER
            {
            public:
                static constexpr LdH::u32f VERSION = 40;

                //LdH::u32f biSize;
                LdH::s32f biWidth;
                LdH::s32f biHeight;
                LdH::u16f biPlanes;
                LdH::u16f biBitCount;
                LdH::u32f biCompression;
                LdH::u32f biSizeImage;
                LdH::s32f biXPelsPerMeter;
                LdH::s32f biYPelsPerMeter;
                LdH::u32f biClrUsed;
                LdH::u32f biClrImportant;

            protected:
                static constexpr auto _struct = LdH::StructSerializer::Struct<BITMAPINFOHEADER>(
                        //LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPINFOHEADER::biSize>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::S32, &BITMAPINFOHEADER::biWidth>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::S32, &BITMAPINFOHEADER::biHeight>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U16, &BITMAPINFOHEADER::biPlanes>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U16, &BITMAPINFOHEADER::biBitCount>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPINFOHEADER::biCompression>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPINFOHEADER::biSizeImage>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::S32, &BITMAPINFOHEADER::biXPelsPerMeter>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::S32, &BITMAPINFOHEADER::biYPelsPerMeter>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPINFOHEADER::biClrUsed>(),
                        LdH::StructSerializer::field<LdH::StructSerializer::SerialType::U32, &BITMAPINFOHEADER::biClrImportant>()
                );

            public:
                static constexpr LdH::size_t RAW_SIZE = _struct.size;

                template<class buffer_t>
                static void serialize(BITMAPINFOHEADER const *object, buffer_t buffer, LdH::size_t offset = 0) noexcept
                {
                    _struct.serialize(object, buffer, offset);
                }

                template<class buffer_t>
                static void deserialize(BITMAPINFOHEADER *object, const buffer_t buffer, LdH::size_t offset = 0) noexcept
                {
                    _struct.deserialize(object, buffer, offset);
                }
            };

            using BITMAPV3HEADER = BITMAPINFOHEADER;


            constexpr LdH::size_t RAW_V_HEADER_MAXSIZE = std::max(BITMAPCOREHEADER::RAW_SIZE, BITMAPINFOHEADER::RAW_SIZE);
            constexpr LdH::size_t RAW_FULL_HEADER_MAXSIZE = RAW_V_HEADER_MAXSIZE + BITMAPFILEHEADER::RAW_SIZE;
        }
    }
}

#endif // ASSIGNMENT_3_IMAGE_ROTATION_BMP_HEADERS_HPP
