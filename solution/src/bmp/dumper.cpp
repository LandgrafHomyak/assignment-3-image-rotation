#include <LdH/int.hpp>
#include "../io.hpp"
#include "headers.hpp"
#include "io.hpp"

namespace ImageRotation
{
    extern "C" save_bmp_result save_bmp(char const *path, pixel_matrix *matrix)
    {
        BMP::Headers::BITMAPFILEHEADER file_header{
                .bfType = BMP::Headers::FILE_MAGIC,
                .bfSize =  (LdH::u32f) (BMP::Headers::BITMAPFILEHEADER::RAW_SIZE + BMP::Headers::BITMAPV3HEADER::RAW_SIZE + (matrix->width * 3 + 3) / 4 * 4 * matrix->height),
                .reserved = 0,
                .bfOffBits = BMP::Headers::BITMAPFILEHEADER::RAW_SIZE + BMP::Headers::BITMAPV3HEADER::RAW_SIZE,
                .headerSize = BMP::Headers::BITMAPV3HEADER::VERSION,
        };
        BMP::Headers::BITMAPV3HEADER bmp_header{
                .biWidth = (LdH::s32f) (matrix->width),
                .biHeight = (LdH::s32f) (matrix->height),
                .biPlanes = 1,
                .biBitCount = 24,
                .biCompression = 0,
                .biSizeImage = (LdH::u32f) ((matrix->width * 3 + 3) / 4 * 4 * matrix->height),
                .biXPelsPerMeter = 10000,
                .biYPelsPerMeter = 10000,
                .biClrUsed = 0,
                .biClrImportant = 0,
        };

        LdH::u8 headers_raw[BMP::Headers::BITMAPFILEHEADER::RAW_SIZE + BMP::Headers::BITMAPV3HEADER::RAW_SIZE];
        BMP::Headers::BITMAPFILEHEADER::serialize(&file_header, headers_raw);
        BMP::Headers::BITMAPV3HEADER::serialize(&bmp_header, headers_raw, BMP::Headers::BITMAPFILEHEADER::RAW_SIZE);

        FILE *fout = fopen(path, "wb");
        if (fout == nullptr)
            return save_bmp_ERR_FOPEN;
        if (std::fwrite(headers_raw, sizeof(LdH::u8), BMP::Headers::BITMAPFILEHEADER::RAW_SIZE + BMP::Headers::BITMAPV3HEADER::RAW_SIZE, fout) != BMP::Headers::BITMAPFILEHEADER::RAW_SIZE + BMP::Headers::BITMAPV3HEADER::RAW_SIZE)
        {
            std::fclose(fout);
            return save_bmp_ERR_IO;
        }

        switch (BMP::pixels_converter_engine<24>::dump_pixels(matrix, fout, BMP::map_pixel24{}))
        {
            case BMP::pixels_converter_engine<24>::dump_pixels_result::dump_pixels_OK:
                break;
            case BMP::pixels_converter_engine<24>::dump_pixels_result::dump_pixels_FWRITE_ERROR:
                std::fclose(fout);
                return save_bmp_ERR_IO;
        }
        std::fclose(fout);
        return save_bmp_OK;
    }
}