#include <LdH/int.hpp>
#include "../io.hpp"
#include "../mem.hpp"
#include "headers.hpp"
#include "io.hpp"

namespace ImageRotation
{
    namespace BMP
    {
        static load_bmp_result parse_pixels(FILE *fin, pixel_matrix *out, LdH::size_t bpp)
        {
            switch (bpp)
            {
                case 1:
                    switch (BMP::pixels_converter_engine<1>::parse_pixels(fin, out, BMP::map_pixel1{}))
                    {
                        case BMP::pixels_converter_engine<1>::parse_pixels_OK:
                            return load_bmp_OK;
                        case BMP::pixels_converter_engine<1>::parse_pixels_FREAD_ERROR:
                            return load_bmp_ERR_IO;
                        case BMP::pixels_converter_engine<1>::parse_pixels_UNEXPECTED_EOF:
                            return load_bmp_FILE_CORRUPTED;
                    }
                case 16:
                    switch (BMP::pixels_converter_engine<16>::parse_pixels(fin, out, BMP::map_pixel16{}))
                    {
                        case BMP::pixels_converter_engine<16>::parse_pixels_OK:
                            return load_bmp_OK;
                        case BMP::pixels_converter_engine<16>::parse_pixels_FREAD_ERROR:
                            return load_bmp_ERR_IO;
                        case BMP::pixels_converter_engine<16>::parse_pixels_UNEXPECTED_EOF:
                            return load_bmp_FILE_CORRUPTED;
                    }
                case 24:
                    switch (BMP::pixels_converter_engine<24>::parse_pixels(fin, out, BMP::map_pixel24{}))
                    {
                        case BMP::pixels_converter_engine<24>::parse_pixels_OK:
                            return load_bmp_OK;
                        case BMP::pixels_converter_engine<24>::parse_pixels_FREAD_ERROR:
                            return load_bmp_ERR_IO;
                        case BMP::pixels_converter_engine<24>::parse_pixels_UNEXPECTED_EOF:
                            return load_bmp_FILE_CORRUPTED;
                    }
                case 32:
                    switch (BMP::pixels_converter_engine<32>::parse_pixels(fin, out, BMP::map_pixel32{}))
                    {
                        case BMP::pixels_converter_engine<32>::parse_pixels_OK:
                            return load_bmp_OK;
                        case BMP::pixels_converter_engine<32>::parse_pixels_FREAD_ERROR:
                            return load_bmp_ERR_IO;
                        case BMP::pixels_converter_engine<32>::parse_pixels_UNEXPECTED_EOF:
                            return load_bmp_FILE_CORRUPTED;
                    }
                default:
                    return load_bmp_UNSUPPORTED_HEADER;
            }
        }

        static load_bmp_result load_bmp_core(FILE *fin, BMP::Headers::BITMAPCOREHEADER *header, pixel_matrix *out)
        {
            if (header->bcWidth == 0)
                return load_bmp_UNSUPPORTED_HEADER;
            if (header->bcHeight == 0)
                return load_bmp_UNSUPPORTED_HEADER;
            if (header->bcPlanes != 1)
                return load_bmp_UNSUPPORTED_HEADER;
            if (header->bcBitCount == 0)
                return load_bmp_UNSUPPORTED_HEADER;

            bool is_matrix_allocated = false;
            *out = alloc_pixel_matrix(header->bcWidth, header->bcHeight, &is_matrix_allocated);

            if (!is_matrix_allocated)
                return load_bmp_ALLOC_MATRIX_ERROR;

            switch (load_bmp_result ret = parse_pixels(fin, out, header->bcBitCount))
            {
                case load_bmp_OK:
                    return load_bmp_OK;
                default:
                    free_pixel_matrix(out);
                    return ret;
            }
        }

        static bool check_bmp_3(BMP::Headers::BITMAPINFOHEADER *header) noexcept
        {
            if (header->biWidth <= 0)
                return true;
            if (header->biHeight <= 0)
                return true;
            if (header->biPlanes != 1)
                return true;
            if (header->biCompression != 0)
                return true;
            if (header->biClrUsed != 0)
                return true;

            return false;
        }

        static load_bmp_result load_bmp_3(FILE *fin, BMP::Headers::BITMAPINFOHEADER *header, pixel_matrix *out)
        {
            if (check_bmp_3(header))
                return load_bmp_UNSUPPORTED_HEADER;

            bool is_matrix_allocated = false;
            *out = alloc_pixel_matrix(header->biWidth, header->biHeight, &is_matrix_allocated);

            if (!is_matrix_allocated)
                return load_bmp_ALLOC_MATRIX_ERROR;

            switch (load_bmp_result ret = parse_pixels(fin, out, header->biBitCount))
            {
                case load_bmp_OK:
                    return load_bmp_OK;
                default:
                    free_pixel_matrix(out);
                    return ret;
            }
        }
    }


    extern "C" load_bmp_result load_bmp(const char *path, pixel_matrix *out)
    {
        load_bmp_result ret;
        BMP::Headers::BITMAPFILEHEADER file_header;
        LdH::u8 file_header_raw[BMP::Headers::RAW_FULL_HEADER_MAXSIZE];
        LdH::size_t header_bytes_read;
        std::FILE *f;
        LdH::u8 const *header_raw;

#       define set_ret_and_exit(LABEL, VALUE) do {ret = (VALUE); goto LABEL; } while (0);((void)0)

        f = fopen(path, "rb");
        if (f == nullptr)
            set_ret_and_exit(RET, load_bmp_ERR_FOPEN);

        header_bytes_read = fread(file_header_raw, sizeof(LdH::u8), BMP::Headers::RAW_FULL_HEADER_MAXSIZE, f);
        if (header_bytes_read < BMP::Headers::BITMAPFILEHEADER::RAW_SIZE)
            set_ret_and_exit(FCLOSE, load_bmp_FILE_CORRUPTED);

        BMP::Headers::BITMAPFILEHEADER::deserialize(&file_header, file_header_raw);
        if (BMP::Headers::BITMAPFILEHEADER::RAW_SIZE + file_header.headerSize - 4 > BMP::Headers::RAW_FULL_HEADER_MAXSIZE)
            set_ret_and_exit(FCLOSE, load_bmp_UNSUPPORTED_HEADER);

        if (file_header.bfType != BMP::Headers::FILE_MAGIC)
            set_ret_and_exit(FCLOSE, load_bmp_BAD_MAGIC);

        if (file_header.reserved != 0)
            set_ret_and_exit(FCLOSE, load_bmp_UNSUPPORTED_HEADER);

        if (0 != std::fseek(f, file_header.bfOffBits, SEEK_SET))
            set_ret_and_exit(FCLOSE, load_bmp_ERR_IO);


        header_raw = file_header_raw + BMP::Headers::BITMAPFILEHEADER::RAW_SIZE;
        switch (file_header.headerSize)
        {
            case BMP::Headers::BITMAPCOREHEADER::VERSION:
            {
                BMP::Headers::BITMAPCOREHEADER header;
                BMP::Headers::BITMAPCOREHEADER::deserialize(&header, header_raw);
                set_ret_and_exit(FCLOSE, BMP::load_bmp_core(f, &header, out));
            }
            case BMP::Headers::BITMAPINFOHEADER::VERSION:
            {
                BMP::Headers::BITMAPINFOHEADER header;
                BMP::Headers::BITMAPINFOHEADER::deserialize(&header, header_raw);
                set_ret_and_exit(FCLOSE, BMP::load_bmp_3(f, &header, out));
            }
            default:
                set_ret_and_exit(FCLOSE, load_bmp_UNSUPPORTED_HEADER);
        }

        // unreachable
#       undef set_ret_and_exit

        FCLOSE:
        std::fclose(f);
        RET:
        return ret;
    }

}