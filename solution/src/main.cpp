#include <cstdio>
#include <cstring>
#include <LdH/int.hpp>

#include "io.hpp"
#include "pixels.hpp"


namespace
{
    struct str_chain_comparator
    {
    private:
        char const *const raw;
    public:
        bool is_matched = false;
        ImageRotation::rotation_angle value = ImageRotation::rotation_angle_0;

        explicit str_chain_comparator(char const *raw) : raw{raw}
        {};

        str_chain_comparator &compare(char const *const templ, ImageRotation::rotation_angle val)
        {
            if (this->is_matched)
                return *this;
            if (std::strcmp(this->raw, templ) == 0)
            {
                this->is_matched = true;
                this->value = val;
            }
            return *this;
        }
    };
}

static int printe(int exit_code, char const *message)
{
    std::printf("%s\n", message);
    return exit_code;
}

int main(const int argc, char const *const *const argv)
{
    if (argc != 4)
        return printe(1, "Wrong arguments count\n");

    auto angle_parser = str_chain_comparator{argv[3]}
            .compare("-270", ImageRotation::rotation_angle_90)
            .compare("-180", ImageRotation::rotation_angle_180)
            .compare("-90", ImageRotation::rotation_angle_270)
            .compare("0", ImageRotation::rotation_angle_0)
            .compare("90", ImageRotation::rotation_angle_90)
            .compare("180", ImageRotation::rotation_angle_180)
            .compare("270", ImageRotation::rotation_angle_270);

    if (!angle_parser.is_matched)
        return printe(1, "Wrong angle");


    ImageRotation::pixel_matrix original_image;
    switch (ImageRotation::load_bmp(argv[1], &original_image))
    {

        case ImageRotation::load_bmp_OK:
            break;
        case ImageRotation::load_bmp_ERR_FOPEN:
            return printe(1, "Input file can't be open. May be it doesn't exist.");
        case ImageRotation::load_bmp_BAD_MAGIC:
            return printe(1, "Input file has bad magic number. Seems it's not .bmp image.");
        case ImageRotation::load_bmp_FILE_CORRUPTED:
            return printe(1, "Input image is corrupted and can't be loaded correctly.");
        case ImageRotation::load_bmp_UNSUPPORTED_HEADER:
            return printe(1, "Input image has unsupported version of format.");
        case ImageRotation::load_bmp_ERR_IO:
            return printe(1, "Error while reading input file.");
        case ImageRotation::load_bmp_ALLOC_MATRIX_ERROR:
            return printe(1, "Memory error.");
    }

    ImageRotation::pixel_matrix rotated_image;
    switch (ImageRotation::rotate(&original_image, angle_parser.value, &rotated_image))
    {
        case ImageRotation::rotate_matrix_OK:
            ImageRotation::free_pixel_matrix(&original_image);
            break;
        case ImageRotation::rotate_matrix_MEMORY_ERROR:
            ImageRotation::free_pixel_matrix(&original_image);
            return printe(1, "Memory error.");
    }

    ImageRotation::save_bmp_result res = ImageRotation::save_bmp(argv[2], &rotated_image);
    ImageRotation::free_pixel_matrix(&rotated_image);
    switch (res)
    {
        case ImageRotation::save_bmp_OK:
            break;
        case ImageRotation::save_bmp_ERR_FOPEN:
            return printe(1, "Output file can't be created.");
        case ImageRotation::save_bmp_ERR_IO:
            return printe(1, "Error while writing to output file.");
    }

    std::printf("Image got rotated.\n");

    return 0;
}
