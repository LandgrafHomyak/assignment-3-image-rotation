#ifndef ASSIGNMENT_3_IMAGE_ROTATION_PIXELS_HPP
# define ASSIGNMENT_3_IMAGE_ROTATION_PIXELS_HPP

#include <LdH/int.hpp>

namespace ImageRotation
{
    template<bool array_impl>
    class _pixel
    {
    };

    template<>
    class _pixel<true>
    {
    private:
        static constexpr LdH::u8 ALPHA_DEFAULT = LdH::max_value<LdH::u8>;
        LdH::u8 data[4];
    public:
        constexpr _pixel(LdH::u8f red, LdH::u8f green, LdH::u8f blue) noexcept: data{red, green, blue, ALPHA_DEFAULT}
        {}

        constexpr _pixel(_pixel<true> const &other) noexcept = default;

        constexpr _pixel(_pixel<true> &&other) noexcept = default;

        constexpr _pixel<true> &operator=(_pixel<true> other)
        {
            this->data[0] = other.data[0];
            this->data[1] = other.data[1];
            this->data[2] = other.data[2];
            this->data[3] = other.data[3];
            return *this;
        }

        [[nodiscard]] LdH::u8f red() const noexcept
        {
            return this->data[0];
        }

        [[nodiscard]] LdH::u8f green() const noexcept
        {
            return this->data[1];
        }

        [[nodiscard]] LdH::u8f blue() const noexcept
        {
            return this->data[2];
        }
    };

    template<>
    class _pixel<false>
    {
    private:
        static constexpr LdH::u8 ALPHA_DEFAULT = LdH::max_value<LdH::u8>;
        LdH::u32 data;
    public:
        constexpr _pixel(LdH::u8f red, LdH::u8f green, LdH::u8f blue) noexcept: data{((LdH::u32) red) | (((LdH::u32) green) << 8) | (((LdH::u32) blue) << 16) | (ALPHA_DEFAULT << 24)}
        {}

        constexpr _pixel(_pixel<false> const &other) noexcept = default;

        constexpr _pixel(_pixel<false> &&other) noexcept = default;

        constexpr _pixel<false> &operator=(_pixel<false> other)
        {
            (const_cast<_pixel<false> *>(this))->data = other.data;
            return *this;
        }


        [[nodiscard]] LdH::u8f red() const noexcept
        {
            return (LdH::u8f) (this->data & 0xFF);
        }

        [[nodiscard]] LdH::u8f green() const noexcept
        {
            return (LdH::u8f) ((this->data >> 8) & 0xFF);
        }

        [[nodiscard]] LdH::u8f blue() const noexcept
        {
            return (LdH::u8f) ((this->data >> 16) & 0xFF);
        }
    };

    using pixel = _pixel<sizeof(_pixel<true>) < sizeof(_pixel<false>)>;


#if 0
    class pixel_row
    {
    public:
        const LdH::size_t size;
    private:
        pixel *buffer;
    public:
        constexpr pixel_row(LdH::size_t size, pixel *storage) noexcept: size{size}, buffer{storage}
        {}

        constexpr pixel const &operator[](LdH::size_t i) const noexcept
        {
            return this->buffer[i];
        }

        constexpr pixel &operator[](LdH::size_t i) noexcept
        {
            return this->buffer[i];
        }
    };
#endif

    class pixel_matrix;


    void free_pixel_matrix(pixel_matrix *);

    class pixel_matrix
    {
    public:
        const LdH::size_t width;
        const LdH::size_t height;
    private:
        pixel *const buffer;

        friend void free_pixel_matrix(pixel_matrix *);

    public:
        constexpr pixel_matrix() noexcept: width{0}, height{0}, buffer{nullptr}
        {}

        constexpr pixel_matrix(LdH::size_t width, LdH::size_t height, pixel *storage) noexcept: width{width}, height{height}, buffer{storage}
        {}

        pixel_matrix &operator=(pixel_matrix other) noexcept
        {
            *const_cast<LdH::size_t *>(&this->width) = other.width;
            *const_cast<LdH::size_t *>(&this->height) = other.height;
            *const_cast<pixel **>(&this->buffer) = other.buffer;
            return *this;
        }


    private:
        class _const_pixel_matrix_row
        {
        private:
            pixel const *const row;

            explicit constexpr _const_pixel_matrix_row(pixel const *p) noexcept: row{p}
            {}

            friend class pixel_matrix;

        public:

            constexpr pixel operator[](LdH::size_t x) const noexcept
            {
                return this->row[x];
            }
        };

        class _mutable_pixel_matrix_row
        {
        private:
            pixel *const row;

            explicit constexpr _mutable_pixel_matrix_row(pixel *p) noexcept: row{p}
            {}

            friend class pixel_matrix;

        public:

            constexpr pixel operator[](LdH::size_t x) const noexcept
            {
                return this->row[x];
            }

            constexpr pixel &operator[](LdH::size_t x) noexcept
            {
                return this->row[x];
            }
        };

    public:
        constexpr _const_pixel_matrix_row operator[](LdH::size_t y) const noexcept
        {
            return _const_pixel_matrix_row{this->buffer + (y * this->width)};
        }

        constexpr _mutable_pixel_matrix_row operator[](LdH::size_t y) noexcept
        {
            return _mutable_pixel_matrix_row{this->buffer + (y * this->width)};
        }
    };

    enum class rotate_matrix_result
    {
        rotate_matrix_OK,
        rotate_matrix_MEMORY_ERROR,
    };

    using rotate_matrix_result::rotate_matrix_OK;
    using rotate_matrix_result::rotate_matrix_MEMORY_ERROR;


    enum class rotation_angle
    {
        rotation_angle_0,
        rotation_angle_90,
        rotation_angle_180,
        rotation_angle_270
    };
    using rotation_angle::rotation_angle_0;
    using rotation_angle::rotation_angle_90;
    using rotation_angle::rotation_angle_180;
    using rotation_angle::rotation_angle_270;

    extern "C" rotate_matrix_result rotate(pixel_matrix const *in, rotation_angle angle, pixel_matrix *out);
}

#endif // ASSIGNMENT_3_IMAGE_ROTATION_PIXELS_HPP
