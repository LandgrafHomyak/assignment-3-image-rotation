#ifndef ASSIGNMENT_3_IMAGE_ROTATION_MEM_HPP
# define ASSIGNMENT_3_IMAGE_ROTATION_MEM_HPP

#include <cstdlib>
#include "pixels.hpp"

namespace ImageRotation
{

    inline pixel_matrix alloc_pixel_matrix(LdH::size_t width, LdH::size_t height, bool *is_ok)
    {
        void *arr = malloc(width * height * sizeof(pixel));
        *is_ok = arr != nullptr;
        return {width, height, (pixel *) arr};
    }

    inline void free_pixel_matrix(pixel_matrix *p)
    {
        free(p->buffer);
        *p = pixel_matrix{0, 0, nullptr};
    }
}

#endif //ASSIGNMENT_3_IMAGE_ROTATION_MEM_HPP
