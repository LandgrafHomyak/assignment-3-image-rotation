#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IO_HPP
# define ASSIGNMENT_3_IMAGE_ROTATION_IO_HPP

#include <cstdio>
#include "pixels.hpp"

namespace ImageRotation
{
    enum class load_bmp_result
    {
        load_bmp_OK,
        load_bmp_ERR_FOPEN,
        load_bmp_BAD_MAGIC,
        load_bmp_FILE_CORRUPTED,
        load_bmp_UNSUPPORTED_HEADER,
        load_bmp_ERR_IO,
        load_bmp_ALLOC_MATRIX_ERROR,
    };
    using load_bmp_result::load_bmp_OK;
    using load_bmp_result::load_bmp_ERR_FOPEN;
    using load_bmp_result::load_bmp_BAD_MAGIC;
    using load_bmp_result::load_bmp_FILE_CORRUPTED;
    using load_bmp_result::load_bmp_UNSUPPORTED_HEADER;
    using load_bmp_result::load_bmp_ERR_IO;
    using load_bmp_result::load_bmp_ALLOC_MATRIX_ERROR;

    extern "C" load_bmp_result load_bmp(char const *path, pixel_matrix *out);


    enum class save_bmp_result
    {
        save_bmp_OK,
        save_bmp_ERR_FOPEN,
        save_bmp_ERR_IO
    };
    using save_bmp_result::save_bmp_OK;
    using save_bmp_result::save_bmp_ERR_FOPEN;
    using save_bmp_result::save_bmp_ERR_IO;


    extern "C" save_bmp_result save_bmp(char const *path, pixel_matrix *matrix);
}

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IO_HPP
